api = 2
core = 8.x

projects[quadstat_core][type] = module
projects[quadstat_core][version] = 1.x-dev

projects[quadstat_misc][type] = module
projects[quadstat_misc][version] = 1.x-dev

projects[honeypot][type] = module
projects[honeypot][version] = 1.30

projects[typed_data][type] = module
projects[typed_data][version] = 1.0-alpha3

projects[social_share][type] = module
projects[social_share][version] = 2.0-beta6

projects[google_analytics][type] = module
projects[google_analytics][version] = 2.4

projects[google_analytics_et][type] = module
projects[google_analytics_et][version] = 2.0-alpha1

projects[admin_toolbar][type] = module  
projects[admin_toolbar][version] = 2.0

projects[field_group][type] = module
projects[field_group][version] = 3.0-rc2

projects[field_permissions][type] = module
projects[field_permissions][version] = 1.0-rc2

projects[markup][type] = module
projects[markup][version] = 1.0-beta1
  
projects[token][type] = module
projects[token][version] = 1.5

projects[features][type] = module
projects[features][version] = 3.8

projects[views_autocomplete_filters][type] = module
projects[views_autocomplete_filters][version] = 1.2
  
projects[webform][type] = module
projects[webform][version] = 5.6

projects[mathjax][type] = module
projects[mathjax][version] = 2.7

projects[config_update][type] = module
projects[config_update][version] = 1.6

projects[libraries][type] = module
projects[libraries][version] = 3.0-alpha1

projects[computed_field][type] = module
projects[computed_field][version] = 2.0

projects[override_node_options][type] = module
projects[override_node_options][version] = 2.4

projects[default_content][type] = module
projects[default_content][version] = 1.0-alpha8

projects[ctools][type] = module
projects[ctools][version] = 3.2

projects[quicktabs][type] = module
projects[quicktabs][version] = 3.0-alpha2

projects[shorten][type] = module
projects[shorten][version] = 1.0

projects[captcha][type] = module
projects[captcha][version] = 1.0-beta4

projects[insert][type] = module
projects[insert][version] = 1.0

projects[smtp][type] = module
projects[smtp][version] = 1.0-beta6

projects[mailsystem][type] = module
projects[mailsystem][version] = 4.2

projects[metatag][type] = module
projects[metatag][version] = 1.11

projects[pathauto][type] = module
projects[pathauto][version] = 1.6

projects[profile][type] = module
projects[profile][version] = 1.0

projects[redirect][type] = module
projects[redirect][version] = 1.5

projects[xmlsitemap][type] = module
projects[xmlsitemap][version] = 1.0-rc1

projects[entity][type] = module
projects[entity][version] = 1.0-rc3

libraries[slickgrid][download][type] = get
libraries[slickgrid][download][url] = "https://github.com/mleibman/SlickGrid/archive/2.1.0.tar.gz"

libraries[codemirror][download][type] = get
libraries[codemirror][download][url] = "https://github.com/codemirror/CodeMirror/archive/5.21.0.tar.gz"

