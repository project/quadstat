Quadstat is a statistical web application framework that can be used to manage datasets and create statistical applications using Drupal.

1. Set the Private directory in settings.php.

2. Set the R binary path at /admin/config/services/quadstat_core.

3. Clear the cache.

Additional documentation is available:

https://www.drupal.org/docs/8/modules/quadstat/quadstat-user-guide

Additional documentation is planned.

The home of this Drupal distribution can be found at:

https://www.drupal.org/project/quadstat

--
Parag Magunia
Current Maintainer
https://www.drupal.org/u/pmagunia
