uuid: 4365f663-d56d-4422-8954-2f244c78bd91
langcode: en
status: true
dependencies: {  }
id: dashboard
label: Welcome
module: quadstat
routes:
  -
    route_name: entity.node.canonical
tips:
  introduction:
    id: introduction
    plugin: text
    label: Welcome
    body: 'Quadstat provides statistical web applications that may be used to study, analyze and manage datasets.'
    weight: 1
  container-dash-all-data:
    id: data-all-public
    plugin: text
    label: 'All Data'
    location: bottom
    body: 'All public data can be found in the first tab. Click a hyperlink to view the dataset.'
    weight: 2
    attributes:
      data-id: ui-id-2
  container-my-data:
    id: data-my
    plugin: text
    label: 'My Data'
    location: bottom
    body: 'Data that you have personally uploaded will appear here for easy reference.'
    weight: 3
    attributes:
      data-id: ui-id-3
  container-analysis-applications:
    id: data-analysis-applications
    plugin: text
    label: 'Analysis Applications'
    location: bottom
    body: 'Use Analysis Application to gather output directly from R.'
    weight: 4
    attributes:
      data-id: ui-id-4
  container-learning-applications:
    id: data-learning-applications
    plugin: text
    label: 'Learning Applications'
    location: bottom
    body: 'Use Learning Application to receive custom help on various educational topics that would be suitable for high school and college statistics students.'
    weight: 5
    attributes:
      data-id: ui-id-5
  container-blog:
    id: blog
    plugin: text
    label: Blog
    location: bottom
    body: 'Read the Quadstat blog to keep up with the latest news about the site.'
    weight: 6
    attributes:
      data-id: ui-id-6
  container-feed:
    id: feed
    plugin: text
    label: 'Recent Queries'
    location: bottom
    body: 'Recent queries submitted by site users appear here. This includes all public output from analysis and learning applications.'
    weight: 7
    attributes:
      data-id: ui-id-7
  container-group:
    id: group
    plugin: text
    label: 'All User Groups'
    location: bottom
    body: 'Every group on Quadstat can be found here. Create a group to add students, private data and output from operations.'
    weight: 8
    attributes:
      data-id: ui-id-8
  container-group-mine:
    id: group-mine
    plugin: text
    label: 'My Groups'
    location: bottom
    body: 'Groups that you specifically belong to appear here.'
    weight: 9
    attributes:
      data-id: ui-id-9
  container-operation-select:
    id: operation-select
    plugin: text
    label: 'Select an App'
    location: bottom
    body: 'Applications are used for statistical analysis. Refine the list by selecting one from the dropdown or type its name in the textfield to the left.'
    weight: 2
    attributes:
      data-class: js-form-item-operation-selected
  container-dataset-select:
    id: dataset-select
    plugin: text
    location: bottom
    label: 'Curated Data'
    body: 'You can choose a curated dataset from the dropdown menu to analyze. Most of these datasets come from the R distribution.'
    weight: 3
    attributes:
      data-class: js-form-item-dataset-selected
  container-inline-dataset-select:
    id: inline-dataset-select
    plugin: text
    location: bottom
    label: 'On-the-fly and Custom Data'
    body: 'Datasets you upload will appear in this dropdown menu. You can also choose <strong>Inline Data</strong> to instantly paste values without an account. Creating a dataset with an account allows you to store and share data for later use.'
    weight: 3
    attributes:
      data-class: view-dataset-selected-custom
  container-curated-data:
    id: curated-data
    location: top
    plugin: text
    label: 'Recent Queries'
    body: 'A list of recent queries sent to R for analysis are presented in this table. Queries sent anonymously are visible to everyone while queries sent as an authenticated user are only visible to the respective owner.'
    weight: 4
    attributes:
      data-id: block-views-block-recent-r-queries-block-1
  container-operation-column-header:
    id: operation-column-header
    location: bottom
    plugin: text
    label: 'View the Data'
    body: 'The contents of a dataset are visible in the grid. You may need to scroll with your mouse to see cells not immediately visibile.'
    weight: 5
    attributes:
      data-class: 'page-node-type-dataset div[id="quadstat-slickgrid"]'
  container-operation-number:
    id: operation-operation-number
    location: top
    plugin: text
    label: 'Application Input'
    body: 'Enter a numeric value as described. This may be an integer or floating point number.'
    weight: 6
    attributes:
      data-class: form-type-number
  container-operation-text-input:
    id: operation-operation-text-input
    location: top
    plugin: text
    label: 'Application Input'
    body: 'Enter a formula or other text as described by the input label.'
    weight: 7
    attributes:
      data-class: form-item-formula
  container-opie-column-header:
    id: opie-column-header
    plugin: text
    label: 'Select Columns for Analysis'
    body: 'Select a column by clicking on its header. This will tell Quadstat which data to use in analysis'
    weight: 8
    attributes:
      data-id: block-datasetpreviewslickgridblock
  container-license:
    id: operation-license
    location: left
    plugin: text
    label: 'Choose a License'
    body: 'If you would like to specify a license that goes along with the analysis you may select one here. If you have a license that is not listed please contact Quadstat.'
    weight: 9
    attributes:
      data-class: form-item-license
  container-clear:
    id: operation-clear
    plugin: text
    location: left
    label: 'Clear Analysis'
    body: 'To declutter the workspace, click <strong>Clear</strong>. This will remove the results highlighted in green from the latest analysis request.'
    weight: 10
    attributes:
      data-class: 'page-node-type-operation input[value="clear"]'
  container-pdf:
    id: operation-pdf
    plugin: text
    label: 'PDF Export'
    location: left
    body: 'If you would like to share the results via PDF, checkmark this box to automatically generate a PDF with the request. This will add additional processing time to the request.'
    weight: 11
    attributes:
      data-class: 'page-node-type-operation input[name="pdf"]'
  container-shorten:
    id: operation-shorten
    plugin: text
    label: 'URL Shorten'
    location: left
    body: 'When Shorten is checkmarked, a webpage hosted by Quadstat will be created based on the results of the analysis. A URL shortlink to that page will be provided to easily share with others on the web.'
    weight: 12
    attributes:
      data-class: 'page-node-type-operation input[name="shorten_option"]'
  container-operation-submit:
    id: operation-submit
    plugin: text
    label: 'Submit Data For Analysis'
    body: 'Once the required number of columns have been selected, click <strong>Submit</strong> to send the current configuration to R for analysis.'
    weight: 13
    attributes:
      data-class: 'page-node-type-operation webform-button--submit'
  container-select-learning:
    id: learning-app-select
    plugin: text
    label: 'Dataset Applications'
    body: 'Select a learning or analysis application from the dropdown to receive output from the dataset listed on this page.'
    weight: 13
    attributes:
      data-class: view-operation-viewer
  container-operation-help:
    id: operation-help
    plugin: text
    label: 'Application Help'
    location: top
    body: 'Each help box will give you instructions on how to use that particular app.'
    weight: 14
    attributes:
      data-class: field--name-field-operation-help
  container-file:
    id: file
    location: bottom
    plugin: text
    label: 'CSV Download'
    body: 'Imported datasets are converted to CSV files which may be downloaded here. This file will be automatically updated when the owner makes changes to a cell in the grid editor. On this page, all data is read-only. If you are the owner of this dataset, click <strong>Edit</strong> from the navigation menu to switch to the grid editor.'
    weight: 15
    attributes:
      data-class: field--name-field-dataset-file
  container-dataset-license:
    id: container-dataset-license
    location: left
    plugin: text
    label: 'Dataset License'
    body: 'The license for the dataset is stated here as selected by the author. If you suspect copyright infringement please contact Quadstat using the nagivation bar at the top of the page.'
    weight: 15
    attributes:
      data-class: field--name-field-dataset-license
  container-body:
    id: body
    location: top
    plugin: text
    label: 'Dataset Documentation'
    body: 'The dataset description helps identify and understand the dataset in context.'
    weight: 16
    attributes:
      data-class: 'page-node-type-dataset field--name-body field__item'
  container-around-the-site:
    id: around-the-site
    plugin: text
    label: 'From Around the site...'
    location: top
    body: 'Visit a random page on Quadstat.net.'
    weight: 17
    attributes:
      data-id: block-views-block-random-content-block-1
  container-shortlink-google:
    id: operation-shortlink-google
    plugin: text
    label: 'Shortlink to App'
    location: top
    body: 'The shortlink found here can be used to share this app page with others in social media.'
    weight: 18
    attributes:
      data-id: edit-this-shortened
  container-feedback-quick:
    id: container-feedback-quick
    plugin: text
    label: 'Quick Feedback'
    location: top
    body: 'Click the <strong>Quick Feedback</strong> button to send feedback or feature requests to the site administrator.'
    weight: 19
    attributes:
      data-id: edit-actions-wizard-next
  container-end-tour:
    id: end-tour
    location: bottom
    plugin: text
    label: 'Technical Support'
    body: 'If you have additional questions or copyright concerns, contact the site administrator using the navigation menu at the top of the page. Guided tours are available on most pages.'
    weight: 20
    attributes:
      title: Tour
